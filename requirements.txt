poetry==1.6.1
poetry-plugin-tweak-dependencies-version==1.5.1
poetry-dynamic-versioning==1.0.1
pip==23.2.1
