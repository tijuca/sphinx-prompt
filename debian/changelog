sphinx-prompt (1.8.0-3) unstable; urgency=medium

  * Team upload.
  * d/.salsa-ci.yml: Adding build/test pipeline
  * autopkgtest: Work around renamed folder

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 29 Nov 2023 17:52:37 +0100

sphinx-prompt (1.8.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 28 Nov 2023 21:05:33 +0100

sphinx-prompt (1.8.0-1) experimental; urgency=medium

  * Team upload.
  * d/watch: Move to git mode
  * New upstream version 1.8.0
  * Rebuild patch queue from patch-queue branch
    Added patch:
    pyproject.toml-Adjust-version-number.patch
  * d/rules: Check used version string in pyproject.toml
    (Closes: #1046426)
  * d/gbp.conf: Add compression type
  * d/control: Add additional needed package python3-poetry
  * d/rules: Work around broken upstream source

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 24 Oct 2023 21:18:38 +0200

sphinx-prompt (1.7.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version 1.7.0
  * d/control: Set version dep >= 7.0.0 for Sphinx

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 25 May 2023 10:13:54 +0200

sphinx-prompt (1.6.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.6.0
  * d/*: Running wrap-and-sort -ast
    No modifications, only reorder content.
  * Drop Debian patch queue
    Not needed as all patches are included within the new upstream source.
  * d/control: Adjust B-D, adding BuildProfileSpecs
  * d/rules: Drop --with option in default target
  * d/rules: Use customized dh_auto_test call to use pytest
  * d/rules: Tune upstream version number in pyproject.toml
  * d/copyright: Update year data of contributions
  * autopkgtest: Tune unittests script a bit

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 19 Feb 2023 11:01:23 +0100

sphinx-prompt (1.5.0-3) unstable; urgency=medium

  * Team upload.

  [ Christian Kastner ]
  * Bump Standards-Version to 4.6.2 (no changes needed)

  [ Carsten Schoenert ]
  * Rebuild patch queue from patch-queue branch
    Added patch:
    Fix-for-new-pygment-version.patch
    (Closes: #1028622)

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 18 Jan 2023 20:06:31 +0100

sphinx-prompt (1.5.0-2) unstable; urgency=medium

  * Team upload.
  * Backport upstream patch to make the tests pass with docutils 0.19.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 17 Dec 2022 22:03:10 +0300

sphinx-prompt (1.5.0-1) unstable; urgency=medium

  * New upstream release.
    - Require python3-sphinx
    - Require python3-pytest
  * Add autopkgtest
  * Update d/watch
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Bump debhelper compatibility to 13
  * Update d/copyright

 -- Christian Kastner <ckk@debian.org>  Thu, 17 Feb 2022 20:03:02 +0100

sphinx-prompt (1.3.0-3) unstable; urgency=medium

  * Source-only upload after going through NEW queue (no changes)

 -- Christian Kastner <ckk@debian.org>  Thu, 04 Feb 2021 22:48:04 +0100

sphinx-prompt (1.3.0-2) unstable; urgency=medium

  * Add missing license to debian/copyright

 -- Christian Kastner <ckk@debian.org>  Wed, 13 Jan 2021 19:09:42 +0100

sphinx-prompt (1.3.0-1) unstable; urgency=medium

  * Initial release. Closes: #977651

 -- Christian Kastner <ckk@debian.org>  Sun, 09 Aug 2020 20:13:35 +0200
